-- Touchscreen interface displaying upcoming train (2 way station)
-- Used on lua operation panel

if event.digiline then

    workspace = {
            {
                command ="clear"
            },
            {
                label="Station: Station B",
                command="addlabel",
                X=0.2, W=1.2, Y=0.2, H=0.8
            },
            {
                label="Line: 1",
                command="addlabel",
                X=3.4, W=1.2, Y=0.2, H=0.8
            },
            {
                label="Interchanges: /",
                command="addlabel",
                X=6.6, W=1.2, Y=0.2, H=0.8
            },
            {
                label="Upcoming trains",
                command="addlabel",
                X=3.4, W=1.2, Y=1.8, H=0.8
            },
            {
                label="Train id",
                command="addlabel",
                X=0.2, W=1.2, Y=2.6, H=0.8
            },
            {
                label="Destination Terminus",
                command="addlabel",
                X=1.8, W=1.2, Y=2.6, H=0.8
            },
            {
                label="Position",
                command="addlabel",
                X=5.0, W=1.2, Y=2.6, H=0.8
            }
        }

    digiline_send("touchscreen", workspace)
    line = {"Macrohard", "Station B", "Station C", "samp"}

    local train_store = F.train_memory.store["1"]
    if not train_store then
        train_store = {}
        F.train_memory.store[1] = train_store
    end
    local no_train = 0
    for train_id, data in pairs(train_store) do
        local pos = F.split(F.train_memory.get_location("1",train_id), ":")
        -- arrv/left/at - station -terminus
        local stationIndex = nil
        for i, v in ipairs(line) do
            if v == pos[2] then
               stationIndex = i
            end
        end
        if pos[3] == "Macrohard" and 2 < stationIndex  then
            no_train = no_train + 1
            workspace = {
            {
                label=train_id,
                command="addlabel",
                X=0.2, W=1.2, Y=2.6+(no_train*0.8), H=0.8
            },
            {
                label=pos[3],
                command="addlabel",
                X=1.8, W=1.2, Y=2.6+(no_train*0.8), H=0.8
            },
            {
                label=pos[1] .. " " .. pos[2],
                command="addlabel",
                X=5.0, W=1.2, Y=2.6+(no_train*0.8), H=0.8
            }}
      elseif pos[3] == "samp" and 2 > stationIndex  then
            no_train = no_train + 1
            workspace = {
            {
                label=train_id,
                command="addlabel",
                X=0.2, W=1.2, Y=2.6+(no_train*0.8), H=0.8
            },
            {
                label=pos[3],
                command="addlabel",
                X=1.8, W=1.2, Y=2.6+(no_train*0.8), H=0.8
            },
            {
                label=pos[1] .. " " .. pos[2],
                command="addlabel",
                X=5.0, W=1.2, Y=2.6+(no_train*0.8), H=0.8
            }}
        elseif stationIndex == 2 and pos[0] == "arriving" then
            no_train = no_train + 1
            workspace = {
            {
                label=train_id,
                command="addlabel",
                X=0.2, W=1.2, Y=2.6+(no_train*0.8), H=0.8
            },
            {
                label=pos[3],
                command="addlabel",
                X=1.8, W=1.2, Y=2.6+(no_train*0.8), H=0.8
            },
            {
                label=pos[1] .. " " .. pos[2],
                command="addlabel",
                X=5.0, W=1.2, Y=2.6+(no_train*0.8), H=0.8
            }}
        end
        if pos[1] == "at" and pos[2] == "Station B" then
                workspace = {
                {
                    label="<= !!! There is a train in station !!! =>",
                    command="addlabel",
                    X=2.6, W=1.2, Y=6.6, H=0.8
                }}
         end


        digiline_send("touchscreen", workspace) 
    end

end
